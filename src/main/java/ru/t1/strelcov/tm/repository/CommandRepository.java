package ru.t1.strelcov.tm.repository;

import ru.t1.strelcov.tm.api.repository.ICommandRepository;
import ru.t1.strelcov.tm.command.AbstractCommand;

import java.util.*;

public final class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    public AbstractCommand getCommandByName(String name) {
        return commands.get(name);
    }

    public AbstractCommand getCommandByArg(String arg) {
        return arguments.get(arg);
    }

    public void add(final AbstractCommand command) {
        final String name = command.name();
        final String arg = command.arg();
        Optional.ofNullable(name).ifPresent((optionalName) -> commands.put(optionalName, command));
        Optional.ofNullable(arg).ifPresent((optionalArg) -> commands.put(optionalArg, command));
    }

}
