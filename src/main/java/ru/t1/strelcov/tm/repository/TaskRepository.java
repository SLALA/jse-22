package ru.t1.strelcov.tm.repository;

import ru.t1.strelcov.tm.api.repository.ITaskRepository;
import ru.t1.strelcov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    @Override
    public void removeAllByProjectId(final String userId, final String projectId) {
        final List<Task> tasksOfProject = findAllByProjectId(userId, projectId);
        list.removeAll(tasksOfProject);
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        return list.stream()
                .filter((task) -> userId.equals(task.getUserId()) && projectId.equals(task.getProjectId()))
                .collect(Collectors.toList());
    }

}
