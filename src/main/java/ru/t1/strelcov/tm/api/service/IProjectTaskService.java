package ru.t1.strelcov.tm.api.service;

import ru.t1.strelcov.tm.model.Project;
import ru.t1.strelcov.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findAllTasksByProjectId(String userId, String projectId);

    Task bindTaskToProject(String userId, String taskId, String projectId);

    Task unbindTaskFromProject(String userId, String taskId);

    Project removeProjectById(String userId, String projectId);

}
