package ru.t1.strelcov.tm.api.service;

import ru.t1.strelcov.tm.api.IBusinessService;
import ru.t1.strelcov.tm.api.IService;
import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IBusinessService<Task> {
}
