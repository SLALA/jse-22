package ru.t1.strelcov.tm.api.service;

import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.model.User;

public interface IAuthService {

    void logout();

    void login(String login, String password);

    String getUserId();

    User getUser();

    void checkRoles(Role... roles);

}
