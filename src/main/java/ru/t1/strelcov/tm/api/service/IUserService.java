package ru.t1.strelcov.tm.api.service;

import ru.t1.strelcov.tm.api.IService;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.model.User;

import java.util.List;

public interface IUserService extends IService<User> {

    User add(String login, String password);

    User add(String login, String password, String email);

    User add(String login, String password, Role role);

    User findByLogin(String name);

    User removeByLogin(String name);

    User updateById(String id, String firstName, String lastName, String middleName, String email);

    User updateByLogin(String login, String firstName, String lastName, String middleName, String email);

    void changePasswordById(String id, String password);

    void lockUserByLogin(String login);

    void unlockUserByLogin(String login);

}
