package ru.t1.strelcov.tm.api.service;

import ru.t1.strelcov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    Collection<AbstractCommand> getCommands();

    Collection<AbstractCommand> getArguments();

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArg(String arg);

    void add(AbstractCommand command);

}
