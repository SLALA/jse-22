package ru.t1.strelcov.tm.api;

import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.List;

public interface IBusinessService<E extends AbstractBusinessEntity> extends IBusinessRepository<E> {

    E add(String userId, String name, String description);

    E updateById(String userId, String id, String name, String description);

    E updateByName(String userId, String oldName, String name, String description);

    E updateByIndex(String userId, Integer index, String name, String description);

    E changeStatusById(String userId, String id, Status status);

    E changeStatusByName(String userId, String oldName, Status status);

    E changeStatusByIndex(String userId, Integer index, Status status);

}
