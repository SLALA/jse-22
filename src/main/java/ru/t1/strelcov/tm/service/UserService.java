package ru.t1.strelcov.tm.service;

import ru.t1.strelcov.tm.api.repository.IUserRepository;
import ru.t1.strelcov.tm.api.service.IUserService;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.exception.empty.*;
import ru.t1.strelcov.tm.exception.entity.*;
import ru.t1.strelcov.tm.model.User;
import ru.t1.strelcov.tm.util.HashUtil;

import java.util.Optional;

public final class UserService extends AbstractService<User> implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public User add(final String login, final String password) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).filter((i) -> !i.isEmpty()).orElseThrow(EmptyPasswordException::new);
        Optional.ofNullable(findByLogin(login)).ifPresent((user) -> {
            throw new UserLoginExistsException();
        });
        final User user;
        final String passwordHash = HashUtil.salt(password);
        user = new User(login, passwordHash);
        add(user);
        return user;
    }

    @Override
    public User add(final String login, final String password, final String email) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).filter((i) -> !i.isEmpty()).orElseThrow(EmptyPasswordException::new);
        Optional.ofNullable(findByLogin(login)).ifPresent((user) -> {
            throw new UserLoginExistsException();
        });
        final User user;
        final String passwordHash = HashUtil.salt(password);
        user = new User(login, passwordHash, email);
        add(user);
        return user;
    }

    @Override
    public User add(final String login, final String password, final Role role) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).filter((i) -> !i.isEmpty()).orElseThrow(EmptyPasswordException::new);
        Optional.ofNullable(findByLogin(login)).ifPresent((user) -> {
            throw new UserLoginExistsException();
        });
        final User user;
        final String passwordHash = HashUtil.salt(password);
        user = new User(login, passwordHash, role);
        add(user);
        return user;
    }

    @Override
    public User findByLogin(final String login) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        return userRepository.findByLogin(login);
    }

    @Override
    public User removeByLogin(final String login) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        return Optional.ofNullable(userRepository.removeByLogin(login)).orElseThrow(UserNotFoundException::new);
    }

    @Override
    public User updateById(final String id, final String firstName, final String lastName, final String middleName, final String email) {
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        final User user = Optional.ofNullable(findById(id)).orElseThrow(UserNotFoundException::new);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        user.setEmail(email);
        return user;
    }

    @Override
    public User updateByLogin(final String login, final String firstName, final String lastName, final String middleName, final String email) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        final User user = Optional.ofNullable(findByLogin(login)).orElseThrow(UserNotFoundException::new);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        user.setEmail(email);
        return user;
    }

    @Override
    public void changePasswordById(final String id, final String password) {
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(password).filter((i) -> !i.isEmpty()).orElseThrow(EmptyPasswordException::new);
        final User user = Optional.ofNullable(findById(id)).orElseThrow(UserNotFoundException::new);
        final String passwordHash = HashUtil.salt(password);
        user.setPasswordHash(passwordHash);
    }

    @Override
    public void lockUserByLogin(final String login) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        final User user = Optional.ofNullable(findByLogin(login)).orElseThrow(UserNotFoundException::new);
        if (user.getRole().equals(Role.ADMIN)) throw new UserAdminLockException();
        user.setLocked(true);
    }

    @Override
    public void unlockUserByLogin(final String login) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        final User user = Optional.ofNullable(findByLogin(login)).orElseThrow(UserNotFoundException::new);
        user.setLocked(false);
    }

}
