package ru.t1.strelcov.tm.service;

import ru.t1.strelcov.tm.api.IBusinessRepository;
import ru.t1.strelcov.tm.api.IBusinessService;
import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.exception.empty.EmptyNameException;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;
import ru.t1.strelcov.tm.exception.entity.EntityNotFoundException;
import ru.t1.strelcov.tm.exception.system.IncorrectIndexException;
import ru.t1.strelcov.tm.model.AbstractBusinessEntity;

import java.util.*;

public abstract class AbstractBusinessService<E extends AbstractBusinessEntity> extends AbstractService<E> implements IBusinessService<E> {

    private final IBusinessRepository<E> entityRepository;

    public AbstractBusinessService(final IBusinessRepository<E> entityRepository) {
        super(entityRepository);
        this.entityRepository = entityRepository;
    }

    @Override
    public List<E> findAll(final String userId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        return entityRepository.findAll(userId);
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        if (!Optional.ofNullable(comparator).isPresent())
            return new ArrayList<>();
        return entityRepository.findAll(userId, comparator);
    }

    @Override
    public void clear(final String userId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        entityRepository.clear(userId);
    }

    @Override
    public E findById(final String userId, final String id) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        return Optional.ofNullable(entityRepository.findById(userId, id)).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public E findByName(final String userId, final String name) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyNameException::new);
        return Optional.ofNullable(entityRepository.findByName(userId, name)).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public E findByIndex(final String userId, final Integer index) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(index).filter((i) -> i >= 0).orElseThrow(IncorrectIndexException::new);
        return Optional.ofNullable(entityRepository.findByIndex(userId, index)).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public E removeById(final String userId, final String id) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        return Optional.ofNullable(entityRepository.removeById(userId, id)).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public E removeByName(final String userId, final String name) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyNameException::new);
        return Optional.ofNullable(entityRepository.removeByName(userId, name)).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public E removeByIndex(final String userId, final Integer index) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(index).filter((i) -> i >= 0).orElseThrow(IncorrectIndexException::new);
        return Optional.ofNullable(entityRepository.removeByIndex(userId, index)).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public E updateById(final String userId, final String id, final String name, final String description) {
        final E entity = Optional.ofNullable(findById(userId, id)).orElseThrow(EntityNotFoundException::new);
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

    @Override
    public E updateByName(final String userId, final String oldName, final String name, final String description) {
        final E entity = Optional.ofNullable(findByName(userId, oldName)).orElseThrow(EntityNotFoundException::new);
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

    @Override
    public E updateByIndex(final String userId, final Integer index, final String name, final String description) {
        final E entity = Optional.ofNullable(findByIndex(userId, index)).orElseThrow(EntityNotFoundException::new);
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

    @Override
    public E changeStatusById(final String userId, final String id, final Status status) {
        final E entity = Optional.ofNullable(findById(userId, id)).orElseThrow(EntityNotFoundException::new);
        entity.setStatus(status);
        if (status == Status.IN_PROGRESS)
            entity.setDateStart(new Date());
        return entity;
    }

    @Override
    public E changeStatusByName(final String userId, final String name, final Status status) {
        final E entity = Optional.ofNullable(findByName(userId, name)).orElseThrow(EntityNotFoundException::new);
        entity.setStatus(status);
        if (status == Status.IN_PROGRESS)
            entity.setDateStart(new Date());
        return entity;
    }

    @Override
    public E changeStatusByIndex(final String userId, final Integer index, final Status status) {
        final E entity = Optional.ofNullable(findByIndex(userId, index)).orElseThrow(EntityNotFoundException::new);
        entity.setStatus(status);
        if (status == Status.IN_PROGRESS)
            entity.setDateStart(new Date());
        return entity;
    }

}
