package ru.t1.strelcov.tm.service;

import ru.t1.strelcov.tm.api.repository.IProjectRepository;
import ru.t1.strelcov.tm.api.repository.ITaskRepository;
import ru.t1.strelcov.tm.api.service.IProjectTaskService;
import ru.t1.strelcov.tm.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;
import ru.t1.strelcov.tm.exception.entity.EmptyProjectIdException;
import ru.t1.strelcov.tm.exception.entity.EmptyTaskIdException;
import ru.t1.strelcov.tm.exception.entity.EntityNotFoundException;
import ru.t1.strelcov.tm.model.Project;
import ru.t1.strelcov.tm.model.Task;

import java.util.List;
import java.util.Optional;

public final class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;

    private final IProjectRepository projectRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Task> findAllTasksByProjectId(final String userId, final String projectId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(projectId).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        final List<Task> tasksOfProject = taskRepository.findAllByProjectId(userId, projectId);
        return tasksOfProject;
    }

    @Override
    public Task bindTaskToProject(final String userId, final String taskId, final String projectId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(taskId).filter((i) -> !i.isEmpty()).orElseThrow(EmptyTaskIdException::new);
        Optional.ofNullable(projectId).filter((i) -> !i.isEmpty()).orElseThrow(EmptyTaskIdException::new);
        final Task task = Optional.ofNullable(taskRepository.findById(userId, taskId)).orElseThrow(EntityNotFoundException::new);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskFromProject(final String userId, final String taskId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(taskId).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        final Task task = Optional.ofNullable(taskRepository.findById(userId, taskId)).orElseThrow(EntityNotFoundException::new);
        task.setProjectId(null);
        return task;
    }

    @Override
    public Project removeProjectById(final String userId, final String projectId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(projectId).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        taskRepository.removeAllByProjectId(userId, projectId);
        final Project project = projectRepository.removeById(userId, projectId);
        return project;
    }

}
