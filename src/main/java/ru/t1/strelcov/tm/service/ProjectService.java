package ru.t1.strelcov.tm.service;

import ru.t1.strelcov.tm.api.repository.IProjectRepository;
import ru.t1.strelcov.tm.api.service.IProjectService;
import ru.t1.strelcov.tm.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.exception.empty.EmptyNameException;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;
import ru.t1.strelcov.tm.model.Project;

import java.util.Optional;


public final class ProjectService extends AbstractBusinessService<Project> implements IProjectService {

    private final IProjectRepository repository;

    public ProjectService(final IProjectRepository repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    public Project add(final String userId, final String name, final String description) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        final Project project;
        if (Optional.ofNullable(description).filter((i) -> !i.isEmpty()).isPresent())
            project = new Project(userId, name, description);
        else
            project = new Project(userId, name);
        add(project);
        return project;
    }

}
