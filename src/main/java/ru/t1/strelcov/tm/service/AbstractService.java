package ru.t1.strelcov.tm.service;

import ru.t1.strelcov.tm.api.IRepository;
import ru.t1.strelcov.tm.api.IService;
import ru.t1.strelcov.tm.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.exception.entity.EntityNotFoundException;
import ru.t1.strelcov.tm.model.AbstractEntity;

import java.util.List;
import java.util.Optional;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    protected final IRepository<E> repository;

    public AbstractService(final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public void add(final E entity) {
        Optional.ofNullable(entity).ifPresent(repository::add);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public void remove(final E entity) {
        Optional.ofNullable(entity).ifPresent(repository::remove);
    }

    @Override
    public E findById(final String id) {
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        return Optional.ofNullable(repository.findById(id)).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public E removeById(final String id) {
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        return Optional.ofNullable(repository.removeById(id)).orElseThrow(EntityNotFoundException::new);
    }

}
