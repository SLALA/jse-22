package ru.t1.strelcov.tm.command.task;

import ru.t1.strelcov.tm.api.service.ITaskService;
import ru.t1.strelcov.tm.exception.entity.TaskNotFoundException;
import ru.t1.strelcov.tm.model.Task;
import ru.t1.strelcov.tm.util.TerminalUtil;

public final class TaskFindByIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-find-by-id";
    }

    @Override
    public String description() {
        return "Find task by id.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        final ITaskService taskService = serviceLocator.getTaskService();
        System.out.println("[FIND TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findById(userId, id);
        showTask(task);
    }

}
