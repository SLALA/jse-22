package ru.t1.strelcov.tm.command.system;

import ru.t1.strelcov.tm.api.service.ICommandService;
import ru.t1.strelcov.tm.command.AbstractCommand;

import java.util.Collection;

public final class DisplayHelpCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-h";
    }

    @Override
    public String name() {
        return "help";
    }

    @Override
    public String description() {
        return "Display list of terminal commands.";
    }

    @Override
    public void execute() {
        final ICommandService commandService = serviceLocator.getCommandService();
        System.out.println("[HELP]");
        final Collection<AbstractCommand> commands = commandService.getCommands();
        for (final AbstractCommand command : commands) {
            System.out.println(command);
        }
    }

}
