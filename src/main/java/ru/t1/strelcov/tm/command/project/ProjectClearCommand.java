package ru.t1.strelcov.tm.command.project;

import ru.t1.strelcov.tm.api.service.IProjectService;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "Create project.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        final IProjectService projectService = serviceLocator.getProjectService();
        System.out.println("[TASK CLEAR]");
        projectService.clear(userId);
    }

}
