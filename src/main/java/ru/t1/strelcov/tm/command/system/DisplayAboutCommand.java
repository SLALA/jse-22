package ru.t1.strelcov.tm.command.system;

import ru.t1.strelcov.tm.command.AbstractCommand;

public final class DisplayAboutCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String name() {
        return "about";
    }

    @Override
    public String description() {
        return "Display developer info.";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Sla La");
        System.out.println("slala@slala.ru");
    }

}
