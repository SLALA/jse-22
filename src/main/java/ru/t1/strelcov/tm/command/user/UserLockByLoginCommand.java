package ru.t1.strelcov.tm.command.user;

import ru.t1.strelcov.tm.api.service.IUserService;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.util.TerminalUtil;

public final class UserLockByLoginCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-lock-by-login";
    }

    @Override
    public String description() {
        return "Lock user by login.";
    }

    @Override
    public void execute() {
        final IUserService userService = serviceLocator.getUserService();
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        userService.lockUserByLogin(login);
    }

    @Override
    public Role[] roles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
