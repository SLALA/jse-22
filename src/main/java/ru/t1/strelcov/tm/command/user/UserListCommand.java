package ru.t1.strelcov.tm.command.user;

import ru.t1.strelcov.tm.api.service.IUserService;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.model.Task;
import ru.t1.strelcov.tm.model.User;
import ru.t1.strelcov.tm.util.TerminalUtil;

import java.util.List;

public final class UserListCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-list";
    }

    @Override
    public String description() {
        return "List users.";
    }

    @Override
    public void execute() {
        final IUserService userService = serviceLocator.getUserService();
        System.out.println("[LIST USERS]");
        final List<User> users = userService.findAll();
        int index = 1;
        for (final User user : users) {
            System.out.println(index + ". " + user);
            index++;
        }
    }

    @Override
    public Role[] roles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
