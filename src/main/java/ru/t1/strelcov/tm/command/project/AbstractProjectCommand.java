package ru.t1.strelcov.tm.command.project;

import ru.t1.strelcov.tm.command.AbstractCommand;
import ru.t1.strelcov.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected void showProject(final Project project) {
        if (project == null) return;
        System.out.println("[Id]: " + project.getId());
        System.out.println("[Name]: " + project.getName());
        System.out.println("[Description]: " + project.getDescription());
        System.out.println("[Status]: " + project.getStatus().getDisplayName());
        System.out.println("[User Id]: " + project.getUserId());
    }

}
