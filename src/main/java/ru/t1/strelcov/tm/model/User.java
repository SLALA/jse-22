package ru.t1.strelcov.tm.model;

import ru.t1.strelcov.tm.enumerated.Role;

import java.util.UUID;

public final class User extends AbstractEntity {

    private String login = "";

    private String passwordHash;

    private String email = "";

    private String firstName = "";

    private String lastName = "";

    private String middleName = "";

    private Role role = Role.USER;

    private Boolean locked = false;

    public User(final String login, final String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public User(final String login, final String passwordHash, final String email) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
    }

    public User(final String login, final String passwordHash, final Role role) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    @Override
    public String toString() {
        return id + (login == null || login.isEmpty() ? "" : (" : " + login));
    }

}
