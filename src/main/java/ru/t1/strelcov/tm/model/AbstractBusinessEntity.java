package ru.t1.strelcov.tm.model;

import ru.t1.strelcov.tm.api.entity.IWBS;
import ru.t1.strelcov.tm.enumerated.Status;

import java.util.Date;

import static ru.t1.strelcov.tm.enumerated.Status.NOT_STARTED;

public abstract class AbstractBusinessEntity extends AbstractEntity implements IWBS {

    private String name = "";

    private String description = "";

    private Status status = NOT_STARTED;

    private Date created = new Date();

    private String userId;

    private Date dateStart;

    public AbstractBusinessEntity() {
    }

    public AbstractBusinessEntity(String userId, String name) {
        this.userId = userId;
        this.name = name;
    }

    public AbstractBusinessEntity(String userId, String name, String description) {
        this.userId = userId;
        this.name = name;
        this.description = description;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return id + " : " + name + " : " + created.toString() + " : " + userId;
    }

}
